# SAMPLE ETHEREUM PROJECT

## Prerequisites

Have installed Node.js
run

```
npm install -g truffle ethereumjs-testrpc
```

Start ethereum test network

```
  testrpc
```

it will run ethereum test network run on ```localhost:8545```

## How to use traffle

```truffle compile``` - to compile

``` truffle test ``` - to trest

``` truffle migerate ``` - to deploy migraton


### ALSO

Use https://remix.ethereum.org/ to test a contract in a "web sandbox"