async function logMyContract (myContract) {
  console.log("")
  console.log("MyContract:")
  console.log("--------------")
  console.log(`BALANCE: ${getBalanceInEth(myContract.address)}`)
  console.log(`startTime=${await myContract.startTime.call()}`)
  console.log(`poolTime=${await myContract.poolTime.call()}`)
  console.log(`threshold=${await myContract.threshold.call()}`)
  console.log(`recipient=${await myContract.recipient.call()}`)
  console.log(`currentTime()=${await myContract.currentTime.call()}`)
  console.log(`isClosed()=${await myContract.isClosed.call()}`)
  console.log("")
 }


 module.exports = {
  logMyContract
 }