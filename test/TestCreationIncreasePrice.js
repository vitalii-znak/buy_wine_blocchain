const WineBottleTmp = artifacts.require('./WineBottle.sol')
const BigNumber = require('bignumber.js');

contract('WineBottle', function(accounts) {
  const addres_0 = accounts[0]
  const addres_1 = accounts[1]
  const addres_2 = accounts[2]
  const addres_3 = accounts[3]
  const addres_4 = accounts[4]


  it(`shoud deploy a WineBottle contract with price of a bottle set to ${0} wei`, async () => {
    const instance = await WineBottleTmp.new({from: addres_0})
    const price = await instance.getPrice.call();
    return assert.equal(price.valueOf(), 0, "0 wasn't the initial price of a bottle");
  } )

  it(`should allow to increase the price of the wine`, async () => {
    const instance = await WineBottleTmp.new({from: addres_0})

    let result
    try {
      result = await new Promise( (resolve, reject) => {
        instance.sendTransaction({
          from: addres_1,
          value: web3.toWei('0.5', 'ether')
        }, (err, result) => {
          if(err){
            return reject(err)
          }
          return resolve(result)
        });
      })
    } catch (err){
      console.error(err);
      assert.isNotOk(err)
    }
    assert.isOk(result, 'succed')
  })

  it(`should not allow to decrease the price of the wine`, async () => {
    const instance = await WineBottleTmp.new({from: addres_0})

    let result_0, result_1, result_2;
    try {
      result_0 = await new Promise( (resolve, reject) => {
        instance.sendTransaction({
          from: addres_2,
          value: web3.toWei('0.5', 'ether')
        }, (err, result) => {
          if(err){
            return reject(err)
          }
          return resolve(result)
        });
      })
    } catch (err){
      console.error(err);
      assert.isNotOk(err)
    }
    try {
      result_1 = await new Promise( (resolve, reject) => {
        instance.sendTransaction({
          from: addres_3,
          value: web3.toWei('4', 'ether')
        }, (err, result) => {
          if(err){
            return reject(err)
          }
          return resolve(result)
        });
      })
    } catch (err){
      assert.isNotOk(err)
    }
    try {
      result_2 = await new Promise( (resolve, reject) => {
        instance.sendTransaction({
          from: addres_4,
          value: web3.toWei('0.1', 'ether')
        }, (err, result) => {
          if(err){
            return reject(err)
          }
          return resolve(result)
        });
      })
    }catch (err){
      assert.isOk(err, 'not allowed to decrease a price')
    }
    assert.isNotOk(result_2, 'price was decresed')
  })


  it(`should transfer the ETH to the current wine bottle owner`, async () => {
    const instance = await WineBottleTmp.new({from: addres_0})
    let result_0, result_1;
    try {
      result_0 = await new Promise( (resolve, reject) => {
        instance.sendTransaction({
          from: addres_4,
          value: web3.toWei('0.5', 'ether')
        }, (err, result) => {
          if(err){
            return reject(err)
          }
          return resolve(result)
        });
      })
    } catch (err){
      console.error(err);
      assert.isNotOk(err)
    }

    const balanceAddres_3 = web3.eth.getBalance(addres_3)
    const balanceAddres_4 = web3.eth.getBalance(addres_4)
    const wineBottlePrice = web3.toWei('1', 'ether')
    try {
      result_1 = await new Promise( (resolve, reject) => {
        instance.sendTransaction({
          from: addres_3,
          value: wineBottlePrice
        }, (err, result) => {
          if(err){
            return reject(err)
          }
          return resolve(result)
        });
      })
    } catch (err){
      console.error(err);
      assert.isNotOk(err)
    }

    const newBalanceAddres_3 = web3.eth.getBalance(addres_3)
    const newBalanceAddres_4 = web3.eth.getBalance(addres_4)

    assert.equal(
      new BigNumber(balanceAddres_4).plus( wineBottlePrice),
      newBalanceAddres_4.toString(),
      "old wine bottle owner recived wrong ammount of coins")

  })

});