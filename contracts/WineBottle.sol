pragma solidity ^0.4.17;

contract WineBottle {
    event bought(address, address, uint256);

    address private creator;
    address private owner;
    uint256 private priceInWei;

    modifier hasMoreMoney(){
        require(priceInWei < msg.value);
        _;
    }

    modifier isNotAnOwner(){
        require(owner != msg.sender);
        _;
    }

    function WineBottle() public {
        creator = msg.sender;
        owner = msg.sender;
        priceInWei = 0;
    }

    function getOwner()
    public
    view
    returns(address){
        return owner;
    }

    function getPrice()
    public
    view
    returns(uint256){
        return priceInWei;
    }

    function()
    public
    payable
    hasMoreMoney {
        address oldOwner = owner;
        oldOwner.transfer(msg.value);
        owner = msg.sender;
        priceInWei = msg.value;

        emit bought(oldOwner, msg.sender, msg.value);
    }

}
